//
//  View.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import SpriteKit

class View: SKView, SimulationView {
  weak var observer: SimulationViewObserver? = nil
  private let simulation: GameScene = GameScene()
  private let pathNode = SKShapeNode(path: CGMutablePath())
  private var vertexNodes: [SKShapeNode] = []
  
  init() {
    super.init(frame: .zero)
    
    ignoresSiblingOrder = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("meh")
  }
  
  override func viewDidMoveToSuperview() {
    presentScene(simulation)
    
    pathNode.strokeColor = .blue
    simulation.addChild(pathNode)
    
    let click = NSClickGestureRecognizer(target: self, action: #selector(handleClick))
    self.addGestureRecognizer(click)
  }

  @objc func handleClick() {
    observer?.didClick()
  }
  
  func addVerticies(_ verticies: [NSPoint]) {
    vertexNodes.forEach { $0.removeFromParent() }
    
    for point in verticies {
      let node = SKShapeNode(circleOfRadius: 5)
      node.fillColor = .white
      node.position = point
      scene?.addChild(node)
      vertexNodes.append(node)
    }
  }
  
  func addPath(_ verticies: [NSPoint]) {
    let path = CGMutablePath()
    
    path.move(to: verticies.first!)
    
    for point in verticies[1...] {
      path.addLine(to: point)
    }
    
    pathNode.path = path
  }
}

private class GameScene: SKScene {
  
  override init() {
    super.init(size: CGSize(width: 500, height: 500))
    
    scaleMode = .aspectFill
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setUpScene() {
    
    let cameraNode = SKCameraNode()
    
    cameraNode.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
    cameraNode.setScale(2.0)
    addChild(cameraNode)
    camera = cameraNode
  }
  
  override func didMove(to view: SKView) {
    self.setUpScene()
  }
  
  
}
