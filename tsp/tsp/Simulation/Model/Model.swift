//
//  Model.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/10/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct Vertex {
  let x: Int
  let y: Int
  
  init(_ x: Int, _ y: Int) {
    self.x = x
    self.y = y
  }
  
  func distance(to vertex: Vertex) -> Double {
    return sqrt(pow(Double(vertex.x - x), 2) + pow(Double(vertex.y - y), 2))
  }
}

struct Individual {
  let order: [Int]
  var fitness: Double = 0
  
  init(order: [Int]) {
    self.order = order
  }
  
  init(size: Int) {
    self.init(order: Array<Int>(0..<size).shuffled())
  }
  
  mutating func setFitness(verticies: [Vertex]) {
    fitness = 0
    
    for i in 0..<order.count-1 {
      let start = verticies[order[i]]
      let end = verticies[order[i+1]]
      
      fitness += start.distance(to: end)
    }
  }
  
  func mutated() -> Individual {
    var mutatedOrder = order
    mutatedOrder.swapAt(Int.random(in: 0..<order.count), Int.random(in: 0..<order.count))
    return Individual(order: mutatedOrder)
  }
  
  // Order 1 Crossover
  
  static func crossover(lhs: Individual, rhs: Individual) -> Individual {
    precondition(lhs.order.count == rhs.order.count, "Individuals must be of the same size")
    
    var crossedOrder = Array<Int>(repeating: -1, count: lhs.order.count)
    let remaining = NSMutableOrderedSet(array: rhs.order)
    let randomIndex1 = Int.random(in: 0..<crossedOrder.count)
    let randomIndex2 = Int.random(in: 0..<crossedOrder.count)
    let startIndex = min(randomIndex1, randomIndex2)
    let endIndex = max(randomIndex1, randomIndex2)
    
    // copy segment from parent 1
    
    for index in startIndex...endIndex {
      crossedOrder[index] = lhs.order[index]
      remaining.remove(lhs.order[index])
    }
    
    // fill in remaining from parent 2 in same order
    
    for i in 0..<lhs.order.count {
      if crossedOrder[i] == -1 {
        crossedOrder[i] = remaining[0] as! Int
        remaining.removeObject(at: 0)
      }
    }
    
    // mutate by chance 
    
    if Int.random(in: 1...100) < 5 {
      crossedOrder.swapAt(Int.random(in: 0..<crossedOrder.count), Int.random(in: 0..<crossedOrder.count))
    }
    
    return Individual(order: crossedOrder)
  }
}


class Model: SimulationModel {
  weak var observer: SimulationModelObserver? = nil
  private let populationSize = 1000
  private let numberOfVerticies = 50
  private var verticies: [Vertex] = []
  private var population: [Individual] = []
  private var timer: Timer? = nil
  
  func start() {
    timer?.invalidate()
    timer = nil
    verticies.removeAll()
    population.removeAll()
    
    for _ in 0..<numberOfVerticies {
      verticies.append(Vertex(Int.random(in: 10...490), Int.random(in: 10...490)))
    }
    
    for _ in 0..<populationSize {
      var individual = Individual(size: verticies.count)
      individual.setFitness(verticies: verticies)
      population.append(individual)
    }
    
    population.sort(by: { $0.fitness < $1.fitness })
    
    observer?.didUpdate(verticies: verticies.map({NSPoint(x: $0.x, y: $0.y)}))
    
    timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { t in
      self.run()
    })
  }
  
  func run() {
    while self.population.count > self.populationSize/2 {
      self.population.removeLast()
    }
    
    while self.population.count < self.populationSize {
      let parent1 = self.population[Int.random(in: 0..<self.populationSize/4)]
      let parent2 = self.population[Int.random(in: 0..<self.populationSize/4)]
      var child = Individual.crossover(lhs: parent1, rhs: parent2)
      child.setFitness(verticies: self.verticies)
      
      self.population.append(child)
    }
    
    self.population.sort(by: { $0.fitness < $1.fitness })
    
    if let best = self.population.first {
      DispatchQueue.main.async {
        self.observer?.didUpdate(optimalPath: best.order.map({ self.verticies[$0] }).map({NSPoint(x: $0.x, y: $0.y)}))
      }
    }
  }
}
