//
//  Presenter.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

class Presenter: SimulationPresenter {
  private var model: SimulationModel
  private var view: SimulationView
  
  init(model: SimulationModel, view: SimulationView) {
    self.model = model
    self.view = view
    
    self.model.observer = self
    self.view.observer = self
  }
  
  func start() {
    model.start()
  }
}


extension Presenter: SimulationModelObserver {
  func didUpdate(verticies: [NSPoint]) {
    self.view.addVerticies(verticies)
  }
  
  func didUpdate(currentPath: [NSPoint]) {
    self.view.addPath(currentPath)
  }
  
  func didUpdate(optimalPath: [NSPoint]) {
    self.view.addPath(optimalPath)
  }
}

extension Presenter: SimulationViewObserver {
  func didClick() {
    model.start()
  }
}
