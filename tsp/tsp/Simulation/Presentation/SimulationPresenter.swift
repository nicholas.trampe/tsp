//
//  SimulationPresenter.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol SimulationPresenter {
  func start()
}
