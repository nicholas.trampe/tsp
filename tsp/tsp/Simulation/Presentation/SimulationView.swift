//
//  SimulationView.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import AppKit

protocol SimulationViewObserver: class {
  func didClick()
}


protocol SimulationView {
  var observer: SimulationViewObserver? { get set }
  
  func addVerticies(_ verticies: [NSPoint])
  func addPath(_ verticies: [NSPoint])
}
