//
//  SimulationModel.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol SimulationModelObserver: class {
  func didUpdate(verticies: [NSPoint])
  func didUpdate(optimalPath: [NSPoint])
  func didUpdate(currentPath: [NSPoint])
}

protocol SimulationModel {
  var observer: SimulationModelObserver? { get set }
  func start()
}
