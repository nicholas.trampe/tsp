//
//  AppDelegate.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//


import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    
}
