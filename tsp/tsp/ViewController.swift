//
//  ViewController.swift
//  tsp
//
//  Created by Nicholas Trampe on 9/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Cocoa
import SpriteKit
import GameplayKit

class ViewController: NSViewController {
  
  private let simulationView = View()
  private let simulationModel = Model()
  private lazy var presenter: Presenter = {
    return Presenter(model: simulationModel, view: simulationView)
  }()
  
  override func loadView() {
    view = simulationView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  override func viewDidAppear() {
    super.viewDidAppear()
    
    presenter.start()
  }
}

